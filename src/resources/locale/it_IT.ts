<?xml version="1.0" ?><!DOCTYPE TS><TS language="it_IT" version="2.1">
<context>
    <name>AboutBox</name>
    <message>
        <location filename="../../gui/aboutbox.cpp" line="35"/>
        <location filename="../../gui/aboutbox.cpp" line="149"/>
        <source>About SpeedCrunch</source>
        <translation>Informazioni su SpeedCrunch</translation>
    </message>
    <message>
        <location filename="../../gui/aboutbox.cpp" line="46"/>
        <source>Maintainer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/aboutbox.cpp" line="48"/>
        <source>Core developers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/aboutbox.cpp" line="56"/>
        <source>Original author</source>
        <translation>Autore originale</translation>
    </message>
    <message>
        <location filename="../../gui/aboutbox.cpp" line="118"/>
        <source>Copyright (C) 2004-2016 The SpeedCrunch developers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/aboutbox.cpp" line="141"/>
        <source>Close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/aboutbox.cpp" line="57"/>
        <source>Math engine</source>
        <translation>Motore matematico</translation>
    </message>
    <message>
        <location filename="../../gui/aboutbox.cpp" line="59"/>
        <source>Thanks</source>
        <translation>Ringraziamenti</translation>
    </message>
    <message>
        <location filename="../../gui/aboutbox.cpp" line="122"/>
        <source>This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version</source>
        <translation>Questo programma è software libero; può essere ridistribuito e/o modificato sotto i termini della GNU General Public License come pubblicata dalla Free Software Foundation; sia la versione 2 della licenza che, a vostra discrezione, qualsiasi versione successiva</translation>
    </message>
    <message>
        <location filename="../../gui/aboutbox.cpp" line="128"/>
        <source>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.</source>
        <translation>Questo programma è stato distribuito nella speranza che possa essere utile ma SENZA NESSSUNA GARANZIA; senza neanche l&apos;implicita garanzia di COMMERCIABILITÀ o IDONEITÀ PER UN PARTICOLARE SCOPO.  Consultare la GNU General Public License per maggiori dettagli.</translation>
    </message>
</context>
<context>
    <name>Book</name>
    <message>
        <location filename="../../core/book.cpp" line="67"/>
        <location filename="../../core/book.cpp" line="84"/>
        <source>Index</source>
        <translation>Indice</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="85"/>
        <source>Algebra</source>
        <translation>Algebra</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="86"/>
        <location filename="../../core/book.cpp" line="116"/>
        <source>Quadratic Equation</source>
        <translation>Equazione di secondo grado</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="87"/>
        <location filename="../../core/book.cpp" line="127"/>
        <source>Logarithmic Base Conversion</source>
        <translation>Conversione in Base Logaritimca</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="88"/>
        <source>Geometry</source>
        <translation>Geometria</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="89"/>
        <location filename="../../core/book.cpp" line="217"/>
        <source>Circle</source>
        <translation>Circonferenza</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="90"/>
        <location filename="../../core/book.cpp" line="292"/>
        <source>Sector</source>
        <translation>Settore</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="91"/>
        <location filename="../../core/book.cpp" line="310"/>
        <source>Sphere</source>
        <translation>Sfera</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="92"/>
        <location filename="../../core/book.cpp" line="255"/>
        <source>Cube</source>
        <translation>Cubo</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="93"/>
        <location filename="../../core/book.cpp" line="236"/>
        <source>Cone</source>
        <translation>Cono</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="94"/>
        <location filename="../../core/book.cpp" line="275"/>
        <source>Cylinder</source>
        <translation>Cilindro</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="95"/>
        <source>Unit Conversions</source>
        <translation>Conversioni unità</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="96"/>
        <location filename="../../core/book.cpp" line="418"/>
        <source>Temperature</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="97"/>
        <source>Electronics</source>
        <translation>Elettronica</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="98"/>
        <location filename="../../core/book.cpp" line="137"/>
        <source>Ohm&apos;s Law</source>
        <translation>Legge di Ohm</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="99"/>
        <location filename="../../core/book.cpp" line="154"/>
        <source>Power</source>
        <translation>Potenza</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="100"/>
        <location filename="../../core/book.cpp" line="178"/>
        <source>Reactance</source>
        <translation>Reattanza</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="101"/>
        <location filename="../../core/book.cpp" line="200"/>
        <source>Resonance</source>
        <translation>Risonanza</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="102"/>
        <location filename="../../core/book.cpp" line="329"/>
        <source>Radio Frequency</source>
        <translation>Frequenza Radio</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="103"/>
        <source>Antennas</source>
        <translation>Antenne</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="104"/>
        <location filename="../../core/book.cpp" line="344"/>
        <source>Characteristic Impedance (coax)</source>
        <translation>Impedenza caratteristica (coassiale)</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="105"/>
        <location filename="../../core/book.cpp" line="359"/>
        <source>Velocity of Propagation (coax)</source>
        <translation>Velocità di propagazione (coassiale)</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="106"/>
        <location filename="../../core/book.cpp" line="376"/>
        <source>Standing Wave Ratio &amp; Return Loss</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="107"/>
        <location filename="../../core/book.cpp" line="402"/>
        <source>Free Space Wavelength</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="142"/>
        <location filename="../../core/book.cpp" line="168"/>
        <source>resistance</source>
        <translation>resistenza</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="143"/>
        <location filename="../../core/book.cpp" line="167"/>
        <source>voltage</source>
        <translation>tensione</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="144"/>
        <location filename="../../core/book.cpp" line="166"/>
        <source>current</source>
        <translation>corrente</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="165"/>
        <source>power</source>
        <translation>potenza</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="186"/>
        <source>inductive reactance</source>
        <translation>reattanza induttiva</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="187"/>
        <source>capacitive reactance</source>
        <translation>reattanza capacitiva</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="188"/>
        <location filename="../../core/book.cpp" line="206"/>
        <source>inductance</source>
        <translation>induttanza</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="189"/>
        <location filename="../../core/book.cpp" line="207"/>
        <source>capacitance</source>
        <translation>capacità</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="190"/>
        <source>frequency</source>
        <translation>frequenza</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="205"/>
        <source>resonance frequency</source>
        <translation>frequenza di risonanza</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="223"/>
        <location filename="../../core/book.cpp" line="280"/>
        <location filename="../../core/book.cpp" line="297"/>
        <location filename="../../core/book.cpp" line="316"/>
        <source>area</source>
        <translation>area</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="224"/>
        <source>perimeter</source>
        <translation>perimetro</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="225"/>
        <location filename="../../core/book.cpp" line="319"/>
        <source>diameter</source>
        <translation>diametro</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="226"/>
        <location filename="../../core/book.cpp" line="243"/>
        <location filename="../../core/book.cpp" line="281"/>
        <location filename="../../core/book.cpp" line="299"/>
        <location filename="../../core/book.cpp" line="318"/>
        <source>radius</source>
        <translation>raggio</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="241"/>
        <location filename="../../core/book.cpp" line="261"/>
        <location filename="../../core/book.cpp" line="279"/>
        <location filename="../../core/book.cpp" line="317"/>
        <source>volume</source>
        <translation>volume</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="242"/>
        <location filename="../../core/book.cpp" line="262"/>
        <source>surface area</source>
        <translation>Area Della Superficie</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="244"/>
        <location filename="../../core/book.cpp" line="282"/>
        <source>height</source>
        <translation>altezza</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="245"/>
        <source>slant height</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="263"/>
        <source>face diagonal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="264"/>
        <source>space diagonal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="265"/>
        <source>edge length</source>
        <translation>lunghezza dello spigolo</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="298"/>
        <source>arc length</source>
        <translation>lunghezza dell&apos;arco</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="300"/>
        <source>central angle (degrees)</source>
        <translation>angolo al centro (gradi)</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="424"/>
        <location filename="../../core/book.cpp" line="425"/>
        <location filename="../../core/book.cpp" line="426"/>
        <source>temperature</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="386"/>
        <source>input power</source>
        <translation>Potenza in ingresso</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="387"/>
        <source>reflected power</source>
        <translation>Potenza riflessa</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="388"/>
        <source>transmitted power</source>
        <translation>Potenza trasmessa</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="389"/>
        <source>return loss</source>
        <translation>Caduta Di Potenza</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="347"/>
        <location filename="../../core/book.cpp" line="366"/>
        <source>dielectric constant</source>
        <translation>costante dielettrica</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="348"/>
        <source>outer conductor&apos;s inner diameter</source>
        <translation>diametro interno del conduttore esterno</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="349"/>
        <source>inner conductor&apos;s outer diameter</source>
        <translation>diametro esterno del conduttore interno</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="365"/>
        <source>speed of light</source>
        <translation>velocità della luce</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="390"/>
        <source>reflection coefficient</source>
        <translation>coefficiente di riflessione</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="391"/>
        <source>standing wave ratio</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="392"/>
        <source>normalized impedance</source>
        <translation>impedenza normalizzata</translation>
    </message>
    <message>
        <location filename="../../core/book.cpp" line="408"/>
        <source>frequency (Hz)</source>
        <translation>frequenza (Hz)</translation>
    </message>
</context>
<context>
    <name>BookDock</name>
    <message>
        <location filename="../../gui/bookdock.cpp" line="71"/>
        <source>Formula Book</source>
        <translation>Formulario</translation>
    </message>
</context>
<context>
    <name>ConstantCompletion</name>
    <message>
        <location filename="../../gui/editor.cpp" line="1071"/>
        <location filename="../../gui/editor.cpp" line="1081"/>
        <location filename="../../gui/editor.cpp" line="1140"/>
        <source>All</source>
        <translation>Tutto</translation>
    </message>
</context>
<context>
    <name>Constants</name>
    <message>
        <location filename="../../core/constants.cpp" line="560"/>
        <source>Universal</source>
        <translation>Universale</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="562"/>
        <source>Archimedes&apos; constant Pi</source>
        <translation>Costante di Archimede Pi</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="563"/>
        <source>Euler&apos;s number</source>
        <translation>Numero di Eulero</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="564"/>
        <source>Golden ratio</source>
        <translation>Sezione aurea</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="567"/>
        <source>General Physics</source>
        <translation>Fisica Generale</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="569"/>
        <source>Characteristic Impedance of Vacuum</source>
        <translation>Impedenza caratteristica del vuoto</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="570"/>
        <source>Dirac&apos;s Constant</source>
        <translation>Costante di Dirac</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="571"/>
        <source>Electric Constant</source>
        <translation>Constante elettrica</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="572"/>
        <source>Gravitation Constant</source>
        <translation>Costante gravitazionale</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="573"/>
        <source>Magnetic Constant</source>
        <translation>Costante magnetica</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="574"/>
        <source>Planck&apos;s Constant</source>
        <translation>Costante di Planck</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="575"/>
        <source>Speed of Light in Vacuum</source>
        <translation>Velocità della luce nel vuoto</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="576"/>
        <source>Standard Gravity</source>
        <translation>Gravità Standard</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="580"/>
        <source>Electromagnetic</source>
        <translation>Elettromagnetico</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="582"/>
        <source>Bohr-Procopiu Magneton</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="583"/>
        <source>Conductance Quantum</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="584"/>
        <source>Coulomb&apos;s Constant</source>
        <translation>Costante di Coulomb</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="585"/>
        <source>Elementary Charge</source>
        <translation>Carica elementare</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="586"/>
        <source>Conventional value of Josephson Constant</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="587"/>
        <source>Josephson Constant</source>
        <translation>Costante di Josephson</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="588"/>
        <source>Magnetic Flux Quantum</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="589"/>
        <source>Nuclear Magneton</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="590"/>
        <source>Resistance Quantum</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="591"/>
        <source>Conventional value of von Klitzing Constant</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="592"/>
        <source>von Klitzing Constant</source>
        <translation>Costante di von Klitzing</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="595"/>
        <source>Atomic &amp; Nuclear</source>
        <translation>Atomico &amp; Nucleare</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="597"/>
        <source>Bohr Radius</source>
        <translation>Raggio di Bohr</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="598"/>
        <source>Fermi Coupling Constant</source>
        <translation>Costante di Fermi</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="599"/>
        <source>Fine-structure Constant</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="600"/>
        <source>Hartree Energy</source>
        <translation>Energia di Hartree</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="601"/>
        <source>Hartree Energy in eV</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="602"/>
        <source>Quantum of Circulation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="603"/>
        <source>Quantum of Circulation times 2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="604"/>
        <source>Rydberg Constant</source>
        <translation>Costante di Rydberg</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="605"/>
        <source>Thomson Cross Section</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="606"/>
        <source>Weak Mixing Angle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="609"/>
        <source>Physico-chemical</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="611"/>
        <source>Atomic Mass Unit</source>
        <translation>Unità di massa atomica</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="612"/>
        <source>Avogadro&apos;s Number</source>
        <translation>Numero di Avogadro</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="613"/>
        <source>Boltzmann Constant</source>
        <translation>Costante di Boltzmann</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="614"/>
        <source>Compton wavelength</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="615"/>
        <source>Compton wavelength over 2 pi</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="616"/>
        <source>Electron volt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="617"/>
        <source>Faraday Constant</source>
        <translation>Costante di Faraday</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="618"/>
        <source>First Radiation Constant</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="619"/>
        <source>First Radiation Constant for Spectral Radiance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="620"/>
        <source>Gas Constant</source>
        <translation>Costante dei gas</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="621"/>
        <source>Loschmidt constant (273.15 K, 100 kPa)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="622"/>
        <source>Loschmidt constant (273.15 K, 101.325 kPa)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="623"/>
        <source>Molar Planck Constant</source>
        <translation>Costante molare di Planck</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="624"/>
        <source>Second Radiation Constant</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="625"/>
        <source>Stefan-Boltzmann Constant</source>
        <translation>Costante di Stefan-Boltzmann</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="626"/>
        <source>{220} Lattice Spacing of Silicon</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="629"/>
        <source>Astronomy</source>
        <translation>Astronomia</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="631"/>
        <source>Astronomical Unit</source>
        <translation>Unità Astronomica</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="632"/>
        <source>Light Year</source>
        <translation>Anno luce</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="633"/>
        <source>Parsec</source>
        <translation>Parsec</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="635"/>
        <source>days</source>
        <translation>giorni</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="637"/>
        <source>Gregorian Year</source>
        <translation>Anno Gregoriano</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="638"/>
        <source>Julian Year</source>
        <translation>Anno Giuliano</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="639"/>
        <source>Sidereal Year</source>
        <translation>Anno siderale</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="640"/>
        <source>Tropical Year</source>
        <translation>Anno tropico</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="642"/>
        <source>Earth Mass</source>
        <translation>Massa terrestre</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="643"/>
        <source>Mean Earth Radius</source>
        <translation>Raggio terrestre medio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="644"/>
        <source>Sun Mass</source>
        <translation>Massa solare</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="645"/>
        <source>Sun Radius</source>
        <translation>Raggio solare</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="646"/>
        <source>Sun Luminosity</source>
        <translation>Luminosità solare</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="650"/>
        <source>Molar Mass</source>
        <translation>Massa molare</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="652"/>
        <source>Aluminium</source>
        <translation>Alluminio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="653"/>
        <source>Antimony</source>
        <translation>Antimonio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="654"/>
        <source>Argon</source>
        <translation>Argon</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="655"/>
        <source>Arsenic</source>
        <translation>Arsenico</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="656"/>
        <source>Barium</source>
        <translation>Bario</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="657"/>
        <source>Beryllium</source>
        <translation>Berillio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="658"/>
        <source>Bismuth</source>
        <translation>Bismuto</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="659"/>
        <source>Boron</source>
        <translation>Boro</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="660"/>
        <source>Bromine</source>
        <translation>Bromine</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="661"/>
        <source>Cadmium</source>
        <translation>Cadmio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="662"/>
        <source>Caesium</source>
        <translation>Cesio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="663"/>
        <source>Calcium</source>
        <translation>Calcio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="664"/>
        <source>Carbon</source>
        <translation>Carbonio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="665"/>
        <source>Cerium</source>
        <translation>Cerio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="666"/>
        <source>Chlorine</source>
        <translation>Cloro</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="667"/>
        <source>Chromium</source>
        <translation>Cromo</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="668"/>
        <source>Cobalt</source>
        <translation>Cobalto</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="669"/>
        <source>Copper</source>
        <translation>Rame</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="670"/>
        <source>Dysprosium</source>
        <translation>Disprosio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="671"/>
        <source>Erbium</source>
        <translation>Erbio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="672"/>
        <source>Europium</source>
        <translation>Europio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="673"/>
        <source>Fluorine</source>
        <translation>Floro</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="674"/>
        <source>Gadolinium</source>
        <translation>Gadolinio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="675"/>
        <source>Gallium</source>
        <translation>Gallio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="676"/>
        <source>Germanium</source>
        <translation>Germanio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="677"/>
        <source>Gold</source>
        <translation>Oro</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="678"/>
        <source>Hafnium</source>
        <translation>Afnio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="679"/>
        <source>Helium</source>
        <translation>Elio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="680"/>
        <source>Holmium</source>
        <translation>Olmio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="681"/>
        <source>Hydrogen</source>
        <translation>Idrogeno</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="682"/>
        <source>Indium</source>
        <translation>Indio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="683"/>
        <source>Iodine</source>
        <translation>Iodio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="684"/>
        <source>Iridium</source>
        <translation>Iridio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="685"/>
        <source>Iron</source>
        <translation>Ferro</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="686"/>
        <source>Krypton</source>
        <translation>Kripton</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="687"/>
        <source>Lanthanum</source>
        <translation>Lantanio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="688"/>
        <source>Lead</source>
        <translation>Piombo</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="689"/>
        <source>Lithium</source>
        <translation>Litio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="690"/>
        <source>Lutetium</source>
        <translation>Lutezio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="691"/>
        <source>Magnesium</source>
        <translation>Magnesio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="692"/>
        <source>Manganese</source>
        <translation>Manganese</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="693"/>
        <source>Mercury</source>
        <translation>Mercurio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="694"/>
        <source>Molybdenum</source>
        <translation>Molibdeno</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="695"/>
        <source>Neodymium</source>
        <translation>Neodimio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="696"/>
        <source>Neon</source>
        <translation>Neon</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="697"/>
        <source>Nickel</source>
        <translation>Nichel</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="698"/>
        <source>Niobium</source>
        <translation>Niobio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="699"/>
        <source>Nitrogen</source>
        <translation>Azoto</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="700"/>
        <source>Osmium</source>
        <translation>Osmio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="701"/>
        <source>Oxygen</source>
        <translation>Ossigeno</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="702"/>
        <source>Palladium</source>
        <translation>Palladio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="703"/>
        <source>Phosphorus</source>
        <translation>Fosforo</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="704"/>
        <source>Platinum</source>
        <translation>Platino</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="705"/>
        <source>Potassium</source>
        <translation>Potassio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="706"/>
        <source>Praseodymium</source>
        <translation>Praseodimio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="707"/>
        <source>Protactinium</source>
        <translation>Protoattinio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="708"/>
        <source>Rhenium</source>
        <translation>Renio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="709"/>
        <source>Rubidium</source>
        <translation>Rubidio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="710"/>
        <source>Ruthenium</source>
        <translation>Rutenio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="711"/>
        <source>Samarium</source>
        <translation>Samario</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="712"/>
        <source>Scandium</source>
        <translation>Scandio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="713"/>
        <source>Selenium</source>
        <translation>Selenio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="714"/>
        <source>Silicon</source>
        <translation>Silicio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="715"/>
        <source>Silver</source>
        <translation>Argento</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="716"/>
        <source>Sodium</source>
        <translation>Sodio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="717"/>
        <source>Strontium</source>
        <translation>Stronzio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="718"/>
        <source>Sulfur</source>
        <translation>Zolfo</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="719"/>
        <source>Tantalum</source>
        <translation>Tantalio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="720"/>
        <source>Tellurium</source>
        <translation>Tellurio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="721"/>
        <source>Terbium</source>
        <translation>Terbio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="722"/>
        <source>Thallium</source>
        <translation>Tallio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="723"/>
        <source>Thorium</source>
        <translation>Torio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="724"/>
        <source>Thulium</source>
        <translation>Tulio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="725"/>
        <source>Tin</source>
        <translation>Stagno</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="726"/>
        <source>Titanium</source>
        <translation>Titanio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="727"/>
        <source>Tungsten</source>
        <translation>Tungsteno</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="728"/>
        <source>Uranium</source>
        <translation>Uranio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="729"/>
        <source>Vanadium</source>
        <translation>Vanadio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="730"/>
        <source>Xenon</source>
        <translation>Xeno</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="731"/>
        <source>Ytterbium</source>
        <translation>Itterbio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="732"/>
        <source>Yttrium</source>
        <translation>Ittrio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="733"/>
        <source>Zinc</source>
        <translation>Zinco</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="734"/>
        <source>Zirconium</source>
        <translation>Zirconio</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="739"/>
        <source>Particle Masses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="741"/>
        <source>Electron Mass</source>
        <translation>Massa dell&apos;elettrone</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="742"/>
        <source>Muon Mass</source>
        <translation>Massa del muone</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="743"/>
        <source>Tau Mass</source>
        <translation>Massa del Tauone</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="744"/>
        <source>Up-Quark Mass</source>
        <translation>Massa del Quark up</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="745"/>
        <source>Down-Quark Mass</source>
        <translation>Massa del Quark down</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="746"/>
        <source>Charm-Quark Mass</source>
        <translation>Massa del Quark charm</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="747"/>
        <source>Strange-Quark Mass</source>
        <translation>Massa del Quark strange</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="748"/>
        <source>Top-Quark Mass</source>
        <translation>Massa del Quark top</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="749"/>
        <source>Bottom-Quark Mass</source>
        <translation>Massa del Quark bottom</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="750"/>
        <source>W-Boson Mass</source>
        <translation>Massa del Bosone W</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="751"/>
        <source>Z-Boson Mass</source>
        <translation>Massa del Bosone Z</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="752"/>
        <source>Higgs-Boson Mass</source>
        <translation>Massa del bosone di Higgs</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="753"/>
        <source>Proton Mass</source>
        <translation>Massa del protone</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="754"/>
        <source>Neutron Mass</source>
        <translation>Massa del neutrone</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="755"/>
        <source>Electron Mass (SI)</source>
        <translation>Massa dell&apos;elettrone (SI)</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="756"/>
        <source>Proton Mass (SI)</source>
        <translation>Massa del protone (SI)</translation>
    </message>
    <message>
        <location filename="../../core/constants.cpp" line="757"/>
        <source>Neutron Mass (SI)</source>
        <translation>Massa del neutrone (SI)</translation>
    </message>
</context>
<context>
    <name>ConstantsDock</name>
    <message>
        <location filename="../../gui/constantsdock.cpp" line="36"/>
        <source>Constants</source>
        <translation>Costanti</translation>
    </message>
</context>
<context>
    <name>ConstantsWidget</name>
    <message>
        <location filename="../../gui/constantswidget.cpp" line="118"/>
        <source>No match found</source>
        <translation>Nessuna corrispondenza</translation>
    </message>
    <message>
        <location filename="../../gui/constantswidget.cpp" line="121"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../../gui/constantswidget.cpp" line="122"/>
        <source>Value</source>
        <translation>Valore</translation>
    </message>
    <message>
        <location filename="../../gui/constantswidget.cpp" line="123"/>
        <source>Unit</source>
        <translation>Unità</translation>
    </message>
    <message>
        <location filename="../../gui/constantswidget.cpp" line="116"/>
        <source>Category</source>
        <translation>Categoria</translation>
    </message>
    <message>
        <location filename="../../gui/constantswidget.cpp" line="117"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="../../gui/constantswidget.cpp" line="164"/>
        <location filename="../../gui/constantswidget.cpp" line="233"/>
        <source>All</source>
        <translation>Tutto</translation>
    </message>
</context>
<context>
    <name>Editor</name>
    <message>
        <location filename="../../gui/editor.cpp" line="360"/>
        <source>User function</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/editor.cpp" line="440"/>
        <source>Argument</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/editor.cpp" line="545"/>
        <source>Current result: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Risultato corrente: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../gui/editor.cpp" line="601"/>
        <source>Selection result: n/a</source>
        <translation>Risultato della selezione</translation>
    </message>
    <message>
        <location filename="../../gui/editor.cpp" line="604"/>
        <source>Selection result: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Risultato della selezione: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>Evaluator</name>
    <message>
        <location filename="../../core/evaluator.cpp" line="1331"/>
        <location filename="../../core/evaluator.cpp" line="1421"/>
        <location filename="../../core/evaluator.cpp" line="1432"/>
        <location filename="../../core/evaluator.cpp" line="1443"/>
        <location filename="../../core/evaluator.cpp" line="1454"/>
        <location filename="../../core/evaluator.cpp" line="1465"/>
        <location filename="../../core/evaluator.cpp" line="1476"/>
        <location filename="../../core/evaluator.cpp" line="1487"/>
        <location filename="../../core/evaluator.cpp" line="1497"/>
        <location filename="../../core/evaluator.cpp" line="1508"/>
        <location filename="../../core/evaluator.cpp" line="1519"/>
        <location filename="../../core/evaluator.cpp" line="1530"/>
        <location filename="../../core/evaluator.cpp" line="1541"/>
        <location filename="../../core/evaluator.cpp" line="1552"/>
        <location filename="../../core/evaluator.cpp" line="1563"/>
        <location filename="../../core/evaluator.cpp" line="1627"/>
        <location filename="../../core/evaluator.cpp" line="1676"/>
        <source>invalid expression</source>
        <translation>espressione non valida</translation>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="75"/>
        <location filename="../../core/evaluator.cpp" line="128"/>
        <source>division by zero</source>
        <translation>divisione per zero</translation>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="84"/>
        <source>too time consuming - computation was rejected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="87"/>
        <location filename="../../core/evaluator.cpp" line="143"/>
        <source>dimension mismatch - quantities with different dimensions cannot be compared, added, etc.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="90"/>
        <source>invalid dimension - operation might require dimensionless arguments</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="93"/>
        <location filename="../../core/evaluator.cpp" line="131"/>
        <source>Computation aborted - encountered numerical instability</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="140"/>
        <source>invalid dimension - function might require dimensionless arguments</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="152"/>
        <source>error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="1383"/>
        <source>compile error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="1569"/>
        <source>unit must not be zero</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="1573"/>
        <source>Conversion failed - dimension mismatch</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="1600"/>
        <location filename="../../core/evaluator.cpp" line="1622"/>
        <source>unknown function or variable</source>
        <translation>variabile o funzione sconosciute</translation>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="1693"/>
        <source>recursion not supported</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="66"/>
        <source>cannot operate on a NaN</source>
        <translation>non posso utilizzare un &quot;non numero&quot;</translation>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="69"/>
        <location filename="../../core/evaluator.cpp" line="119"/>
        <source>underflow - tiny result is out of SpeedCrunch&apos;s number range</source>
        <translation>underflow - il valore risultante è troppo piccolo per essere rappresentato da SpeedCrunch</translation>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="72"/>
        <location filename="../../core/evaluator.cpp" line="116"/>
        <source>overflow - huge result is out of SpeedCrunch&apos;s number range</source>
        <translation>overflow - il valore risultante è troppo grande per essere rappresentato da SpeedCrunch</translation>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="78"/>
        <location filename="../../core/evaluator.cpp" line="122"/>
        <source>overflow - logic result exceeds maximum of 256 bits</source>
        <translation>overflow - il risultato logico eccede il valore massimo di 256 bits</translation>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="81"/>
        <source>overflow - integer result exceeds maximum limit for integers</source>
        <translation>overflow - il risultato in formato intero eccede il valore massimo per gli interi</translation>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="110"/>
        <location filename="../../core/evaluator.cpp" line="1687"/>
        <source>wrong number of arguments</source>
        <translation>numero di argomenti errato</translation>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="113"/>
        <source>does not take NaN as an argument</source>
        <translation>non sono accettati NaN (Not-A-Number) come argomento</translation>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="125"/>
        <source>result out of range</source>
        <translation>risultato fuori range</translation>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="134"/>
        <source>undefined for argument domain</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="137"/>
        <source>computation too expensive</source>
        <translation>calcolo troppo complesso</translation>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="149"/>
        <source>internal error, please report a bug</source>
        <translation>errore interno, prego riportare l&apos;anomalia</translation>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="1755"/>
        <location filename="../../core/evaluator.cpp" line="1776"/>
        <source>%1 is a reserved name, please choose another</source>
        <translation>%1 è un nome risevato, scegliere un altro</translation>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="1762"/>
        <source>%1 is a variable name, please choose another or delete the variable</source>
        <translation>%1 é il nome di una variabile, scegliere un altro nome o eliminare la variabile</translation>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="1771"/>
        <source>argument %1 is used more than once</source>
        <translation>L&apos;argomento %1 è usato più di una volta</translation>
    </message>
    <message>
        <location filename="../../core/evaluator.cpp" line="1792"/>
        <source>%1 is a user function name, please choose another or delete the function</source>
        <translation>%1 è una funzione definita dall&apos;utente, sceglierne un&apos;altra o eliminare la funzione</translation>
    </message>
</context>
<context>
    <name>FunctionRepo</name>
    <message>
        <location filename="../../core/functions.cpp" line="992"/>
        <source>max; trials; probability</source>
        <translation>Massimo; Processi; Probabilità</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="993"/>
        <location filename="../../core/functions.cpp" line="995"/>
        <source>trials; probability</source>
        <translation>Processi; Probabilità</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="994"/>
        <source>hits; trials; probability</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="996"/>
        <source>max; total; hits; trials</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="997"/>
        <location filename="../../core/functions.cpp" line="999"/>
        <source>total; hits; trials</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="998"/>
        <source>count; total; hits; trials</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1000"/>
        <source>dividend; divisor</source>
        <translation>dividendo; divisore</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1001"/>
        <location filename="../../core/functions.cpp" line="1002"/>
        <source>x; exponent_bits; significand_bits [; exponent_bias]</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1003"/>
        <source>base; x</source>
        <translation>base; x</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1004"/>
        <location filename="../../core/functions.cpp" line="1011"/>
        <location filename="../../core/functions.cpp" line="1012"/>
        <location filename="../../core/functions.cpp" line="1013"/>
        <source>n; bits</source>
        <translation>n; bits</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1005"/>
        <source>value; modulo</source>
        <translation>valore; modulo</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1006"/>
        <location filename="../../core/functions.cpp" line="1008"/>
        <source>events; average_events</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1007"/>
        <location filename="../../core/functions.cpp" line="1009"/>
        <source>average_events</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1010"/>
        <source>x [; precision]</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1018"/>
        <source>Absolute Value</source>
        <translation>Valore Assoluto</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1019"/>
        <source>Absolute Deviation</source>
        <translation>Deviazione Assoluta</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1020"/>
        <source>Arc Cosine</source>
        <translation>Arcocoseno</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1021"/>
        <source>Logical AND</source>
        <translation>Congiunzione logica</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1022"/>
        <source>Area Hyperbolic Cosine</source>
        <translation>Area Iperbolica Del Coseno</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1023"/>
        <source>Area Hyperbolic Sine</source>
        <translation>Area Iperbolica Del Seno</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1024"/>
        <source>Area Hyperbolic Tangent</source>
        <translation>Area Iperbolica Della Tangente</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1025"/>
        <source>Arc Sine</source>
        <translation>Arcoseno</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1026"/>
        <source>Arc Tangent</source>
        <translation>Arcotangente</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1027"/>
        <source>Average (Arithmetic Mean)</source>
        <translation>Media (Media Aritmetica)</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1029"/>
        <source>Binomial Cumulative Distribution Function</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1030"/>
        <source>Binomial Distribution Mean</source>
        <translation>Media Della Distribuzione Binomiale</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1031"/>
        <source>Binomial Probability Mass Function</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1032"/>
        <source>Binomial Distribution Variance</source>
        <translation>Varianza Della Distribuzione Binomiale</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1033"/>
        <source>Cube Root</source>
        <translation>Radice Cubica</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1034"/>
        <source>Ceiling</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1035"/>
        <source>Cosine</source>
        <translation>Coseno</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1036"/>
        <source>Hyperbolic Cosine</source>
        <translation>Coseno Iperbolico</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1037"/>
        <source>Cotangent</source>
        <translation>Cotangente</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1038"/>
        <source>Cosecant</source>
        <translation>Cosecante</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1040"/>
        <source>Degrees of Arc</source>
        <translation>Gradi sessagesimali</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1041"/>
        <source>Error Function</source>
        <translation>Funzione d&apos;errore</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1042"/>
        <source>Complementary Error Function</source>
        <translation>Funzione d&apos;errore complementare</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1043"/>
        <source>Exponential</source>
        <translation>Esponenziale</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1044"/>
        <source>Floor</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1045"/>
        <source>Fractional Part</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1046"/>
        <source>Extension of Factorials [= (x-1)!]</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1047"/>
        <source>Greatest Common Divisor</source>
        <translation>Massimo Comune Divisore</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1048"/>
        <source>Geometric Mean</source>
        <translation>Media Geometrica</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1050"/>
        <source>Hypergeometric Cumulative Distribution Function</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1051"/>
        <source>Hypergeometric Distribution Mean</source>
        <translation>Media Della Distribuzione Ipergeometrica</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1052"/>
        <source>Hypergeometric Probability Mass Function</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1053"/>
        <source>Hypergeometric Distribution Variance</source>
        <translation>Varianza Della Distribuzione Ipergeometrica</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1054"/>
        <source>Integer Quotient</source>
        <translation>Quoziente Intero</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1055"/>
        <source>Integer Part</source>
        <translation>Parte intera</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1056"/>
        <source>Decode IEEE-754 Binary Value</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1057"/>
        <source>Encode IEEE-754 Binary Value</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1058"/>
        <source>Decode 16-bit Half-Precision Value</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1059"/>
        <source>Encode 16-bit Half-Precision Value</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1060"/>
        <source>Decode 32-bit Single-Precision Value</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1061"/>
        <source>Encode 32-bit Single-Precision Value</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1062"/>
        <source>Decode 64-bit Double-Precision Value</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1063"/>
        <source>Encode 64-bit Double-Precision Value</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1064"/>
        <source>Decode 128-bit Quad-Precision Value</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1065"/>
        <source>Encode 128-bit Quad-Precision Value</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1066"/>
        <source>Binary Logarithm</source>
        <translation>Logaritmo Binario</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1067"/>
        <source>Common Logarithm</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1068"/>
        <source>Natural Logarithm</source>
        <translation>Logaritmo naturale</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1070"/>
        <source>Logarithm to Arbitrary Base</source>
        <translation>Logaritmo A Base Arbitraria</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1071"/>
        <source>Mask to a bit size</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1072"/>
        <source>Maximum</source>
        <translation>Massimo</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1073"/>
        <source>Median Value (50th Percentile)</source>
        <translation>Valore Medio (50simo Percentile)</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1074"/>
        <source>Minimum</source>
        <translation>Minimo</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1075"/>
        <source>Modulo</source>
        <translation>Modulo</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1076"/>
        <source>Combination (Binomial Coefficient)</source>
        <translation>Combinazione (Coefficiente Binomiale)</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1077"/>
        <source>Logical NOT</source>
        <translation>Negazione logica</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1078"/>
        <source>Permutation (Arrangement)</source>
        <translation>Permutazione (Arrangiamento)</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1028"/>
        <source>Convert to Binary Representation</source>
        <translation>Converti In Rappresentazione Binaria</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1039"/>
        <source>Convert to Decimal Representation</source>
        <translation>Converti In Rappresentazione Decimale</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1049"/>
        <source>Convert to Hexadecimal Representation</source>
        <translation>Converti In Rappresentazione Esadecimale</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1079"/>
        <source>Convert to Octal Representation</source>
        <translation>Converti In Rappresentazione Ottale</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1080"/>
        <source>Logical OR</source>
        <translation>Disgiunzione Logica</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1081"/>
        <source>Poissonian Cumulative Distribution Function</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1082"/>
        <source>Poissonian Distribution Mean</source>
        <translation>Media Della Distribuzione di Poisson</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1083"/>
        <source>Poissonian Probability Mass Function</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1084"/>
        <source>Poissonian Distribution Variance</source>
        <translation>Varianza Della Distribuzione di Poisson</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1085"/>
        <source>Product</source>
        <translation>Prodotto</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1086"/>
        <source>Radians</source>
        <translation>Radianti</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1087"/>
        <source>Rounding</source>
        <translation>Arrotondamento</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1088"/>
        <source>Secant</source>
        <translation>Secante</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1089"/>
        <source>Arithmetic Shift Left</source>
        <translation>Shift Aritmetico A Sinistra</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1090"/>
        <source>Arithmetic Shift Right</source>
        <translation>Shift Aritmetico A Destra</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1091"/>
        <source>Signum</source>
        <translation>Segno</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1092"/>
        <source>Sine</source>
        <translation>Seno</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1093"/>
        <source>Hyperbolic Sine</source>
        <translation>Seno Iperbolico</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1094"/>
        <source>Square Root</source>
        <translation>Radice Quadrata</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1095"/>
        <source>Standard Deviation (Square Root of Variance)</source>
        <translation>Deviazione Standard (Radice Quadrata Della Varianza)</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1096"/>
        <source>Sum</source>
        <translation>Somma</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1097"/>
        <source>Tangent</source>
        <translation>Tangente</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1098"/>
        <source>Hyperbolic Tangent</source>
        <translation>Tangente Iperbolica</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1099"/>
        <source>Truncation</source>
        <translation>Troncamento</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1100"/>
        <source>Sign-extend a value</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1101"/>
        <source>Variance</source>
        <translation>Varianza</translation>
    </message>
    <message>
        <location filename="../../core/functions.cpp" line="1102"/>
        <source>Logical XOR</source>
        <translation>Disgiunzione Esclusiva</translation>
    </message>
</context>
<context>
    <name>FunctionsDock</name>
    <message>
        <location filename="../../gui/functionsdock.cpp" line="36"/>
        <source>Functions</source>
        <translation>Funzioni</translation>
    </message>
</context>
<context>
    <name>FunctionsWidget</name>
    <message>
        <location filename="../../gui/functionswidget.cpp" line="141"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../../gui/functionswidget.cpp" line="145"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="../../gui/functionswidget.cpp" line="146"/>
        <source>No match found</source>
        <translation>Nessuna corrispondenza trovata</translation>
    </message>
    <message>
        <location filename="../../gui/functionswidget.cpp" line="140"/>
        <source>Identifier</source>
        <translation>Identificatore</translation>
    </message>
</context>
<context>
    <name>HistoryDock</name>
    <message>
        <location filename="../../gui/historydock.cpp" line="37"/>
        <source>History</source>
        <translation>Cronologia</translation>
    </message>
</context>
<context>
    <name>Keypad</name>
    <message>
        <location filename="../../gui/keypad.cpp" line="167"/>
        <source>Inverse cosine</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/keypad.cpp" line="168"/>
        <source>The last result</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/keypad.cpp" line="169"/>
        <source>Inverse sine</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/keypad.cpp" line="170"/>
        <source>Inverse tangent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/keypad.cpp" line="171"/>
        <source>Clear expression</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/keypad.cpp" line="172"/>
        <source>Cosine</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/keypad.cpp" line="173"/>
        <source>Scientific notation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/keypad.cpp" line="174"/>
        <source>Exponential</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/keypad.cpp" line="175"/>
        <source>Natural logarithm</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/keypad.cpp" line="176"/>
        <source>Sine</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/keypad.cpp" line="177"/>
        <source>Square root</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/keypad.cpp" line="178"/>
        <source>Tangent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/keypad.cpp" line="179"/>
        <source>Assign variable x</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/keypad.cpp" line="180"/>
        <source>The variable x</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="266"/>
        <location filename="../../gui/mainwindow.cpp" line="1585"/>
        <source>Radian</source>
        <translation>Radianti</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="266"/>
        <location filename="../../gui/mainwindow.cpp" line="1259"/>
        <source>Degree</source>
        <translation>Gradi</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="270"/>
        <location filename="../../gui/mainwindow.cpp" line="1924"/>
        <source>Binary</source>
        <translation>Binario</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="271"/>
        <location filename="../../gui/mainwindow.cpp" line="1963"/>
        <source>Octal</source>
        <translation>Ottale</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="272"/>
        <location filename="../../gui/mainwindow.cpp" line="1955"/>
        <source>Hexadecimal</source>
        <translation>Esadecimale</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="273"/>
        <location filename="../../gui/mainwindow.cpp" line="1940"/>
        <source>Fixed decimal</source>
        <translation>Decimale a precisione fissa</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="274"/>
        <location filename="../../gui/mainwindow.cpp" line="1932"/>
        <source>Engineering decimal</source>
        <translation>Decimale ingegneristico</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="275"/>
        <location filename="../../gui/mainwindow.cpp" line="1971"/>
        <source>Scientific decimal</source>
        <translation>Decimale scientifico</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="276"/>
        <location filename="../../gui/mainwindow.cpp" line="1947"/>
        <source>General decimal</source>
        <translation>Decimale generale</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="283"/>
        <source>Angle unit</source>
        <translation>Unità d&apos;Angolo</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="284"/>
        <source>Result format</source>
        <translation>Formato del risultato</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="292"/>
        <source>&amp;Import...</source>
        <translation>&amp;Importa...</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="293"/>
        <source>&amp;Load...</source>
        <translation>&amp;Carica...</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="294"/>
        <source>&amp;Quit</source>
        <translation>&amp;Esci</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="295"/>
        <source>&amp;Save...</source>
        <translation>&amp;Salva...</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="297"/>
        <source>Clear E&amp;xpression</source>
        <translation>Cancella l&apos;E&amp;spressione</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="298"/>
        <source>Clear &amp;History</source>
        <translation>Cancella la &amp;Cronologia</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="299"/>
        <source>Copy Last &amp;Result</source>
        <translation>Copia l&apos;Ultimo &amp;Risultato</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="300"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copia</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="301"/>
        <source>&amp;Paste</source>
        <translation>&amp;Incolla</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="302"/>
        <source>&amp;Select Expression</source>
        <translation>&amp;Seleziona l&apos;Espressione</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="303"/>
        <source>&amp;Wrap Selection in Parentheses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="305"/>
        <source>&amp;Constants</source>
        <translation>&amp;Costanti</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="306"/>
        <source>F&amp;ull Screen Mode</source>
        <translation>Modalità &amp;Schermo Intero</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="307"/>
        <source>&amp;Functions</source>
        <translation>&amp;Funzioni</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="308"/>
        <source>&amp;History</source>
        <translation>&amp;Cronologia</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="311"/>
        <source>&amp;Status Bar</source>
        <translation>Barra di &amp;Stato</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="312"/>
        <source>&amp;Variables</source>
        <translation>&amp;Variabili</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="313"/>
        <source>Bitfield</source>
        <translation>Bitfield</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="314"/>
        <source>Use&amp;r Functions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="316"/>
        <source>&amp;Degree</source>
        <translation>&amp;Gradi</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="317"/>
        <source>&amp;Radian</source>
        <translation>&amp;Radianti</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="319"/>
        <source>Automatic Result &amp;Reuse</source>
        <translation>&amp;Riutilizza automaticamente il risultato</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="320"/>
        <source>Automatic &amp;Completion</source>
        <translation>&amp;Completamento Automatico</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="321"/>
        <source>&amp;Partial Results</source>
        <translation>Risultato &amp;Parziale</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="322"/>
        <source>Save &amp;History on Exit</source>
        <translation>Salva la &amp;Cronologia quando termina</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="323"/>
        <source>Save &amp;Window Positon on Exit</source>
        <translation>Salva la posizione della finestra all&apos;uscita</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="324"/>
        <source>Syntax &amp;Highlighting</source>
        <translation>&amp;Evidenziatore Sintattico</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="329"/>
        <source>Leave &amp;Last Expression</source>
        <translation>Mantieni l&apos;ultima espressione</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="330"/>
        <source>Automatic &amp;Result to Clipboard</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="334"/>
        <source>&amp;Comma</source>
        <translation>&amp;Virgola</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="335"/>
        <source>&amp;System Default</source>
        <translation>Predefinita di &amp;Sistema</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="336"/>
        <source>&amp;Dot</source>
        <translation>&amp;Punto</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="344"/>
        <source>&amp;Binary</source>
        <translation>&amp;Binario</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="345"/>
        <source>&amp;Engineering</source>
        <translation>&amp;Ingegneristica</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="346"/>
        <source>&amp;Fixed Decimal</source>
        <translation>&amp;Decimale a precisione Fissa</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="347"/>
        <source>&amp;General</source>
        <translation>&amp;Generale</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="348"/>
        <source>&amp;Hexadecimal</source>
        <translation>&amp;Esadecimale</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="349"/>
        <source>&amp;Octal</source>
        <translation>&amp;Ottale</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="350"/>
        <source>&amp;Scientific</source>
        <translation>&amp;Scientifica</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="354"/>
        <source>User &amp;Manual</source>
        <translation>&amp;Manuale Utente</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1097"/>
        <source>Type an expression here</source>
        <translation>Digita un&apos;espressione qui</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="351"/>
        <source>&amp;Font...</source>
        <translation>&amp;Font</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="352"/>
        <source>&amp;Language...</source>
        <translation>&amp;Lingua...</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="566"/>
        <source>&amp;Session</source>
        <translation>&amp;Sessione</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="568"/>
        <source>&amp;Edit</source>
        <translation>&amp;Modifica</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="569"/>
        <source>&amp;View</source>
        <translation>&amp;Visualizza</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="570"/>
        <source>Se&amp;ttings</source>
        <translation>Im&amp;postazioni</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="571"/>
        <source>Result &amp;Format</source>
        <translation>&amp;Formato del Risultato</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="573"/>
        <source>&amp;Decimal</source>
        <translation>&amp;Decimale</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="575"/>
        <source>&amp;Angle Unit</source>
        <translation>Unità d&apos;&amp;Angolo</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="576"/>
        <source>&amp;Behavior</source>
        <translation>&amp;Comportamento</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="572"/>
        <source>Radix &amp;Character</source>
        <translation>&amp;Carattere di Radice</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="309"/>
        <source>&amp;Keypad</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="318"/>
        <source>Always on &amp;Top</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="325"/>
        <source>Disabled</source>
        <translation>Disabilitato</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="326"/>
        <source>Small Space</source>
        <translation>Spazio Piccolo</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="327"/>
        <source>Medium Space</source>
        <translation>Spazio Medio</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="328"/>
        <source>Large Space</source>
        <translation>Spazio Grande</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="331"/>
        <source>Detect &amp;All Radix Characters</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="332"/>
        <source>&amp;Strict Digit Groups Detection</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="333"/>
        <source>Enable Complex Numbers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="337"/>
        <source>&amp;0 Digits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="338"/>
        <source>&amp;15 Digits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="339"/>
        <source>&amp;2 Digits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="340"/>
        <source>&amp;3 Digits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="341"/>
        <source>&amp;50 Digits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="342"/>
        <source>&amp;8 Digits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="355"/>
        <source>Context Help</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="360"/>
        <source>About &amp;SpeedCrunch</source>
        <translation>A proposito di &amp;SpeedCrunch</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="577"/>
        <source>&amp;Display</source>
        <translation>&amp;Display</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="579"/>
        <source>&amp;Help</source>
        <translation>&amp;Aiuto</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="580"/>
        <source>Digit Grouping</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1343"/>
        <source>File %1 is not a valid session</source>
        <translation>Il file %1 non contiene una sessione valida</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1345"/>
        <source>Load Session</source>
        <translation>Carica Sessione</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1351"/>
        <location filename="../../gui/mainwindow.cpp" line="1396"/>
        <location filename="../../gui/mainwindow.cpp" line="1417"/>
        <location filename="../../gui/mainwindow.cpp" line="1453"/>
        <location filename="../../gui/mainwindow.cpp" line="1610"/>
        <location filename="../../gui/mainwindow.cpp" line="1631"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1351"/>
        <location filename="../../gui/mainwindow.cpp" line="1417"/>
        <source>Can&apos;t read from file %1</source>
        <translation>Impossibile leggere dal file %1</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1357"/>
        <source>Merge session being loaded with current session?
If no, current variables and display will be cleared.</source>
        <translation>Si vuole mischiare la sessione da caricare con quella corrente?
In caso di risposta negativa variabili e visualizzazione corrente saranno pulite.</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1410"/>
        <source>All Files (*)</source>
        <translation>Tutti i file (*)</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1411"/>
        <source>Import Session</source>
        <translation>Importa Sessione</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1422"/>
        <source>Merge session being imported with current session?
If no, current variables and display will be cleared.</source>
        <translation>Si vuole mischiare la sessione da importare con quella corrente?In caso di risposta negativa variabili e visualizzazione corrente saranno pulite.</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1453"/>
        <source>Ignore error?</source>
        <translation>Ignorare l&apos;errore?</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1390"/>
        <source>Save Session</source>
        <translation>Salva la Sessione</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1396"/>
        <location filename="../../gui/mainwindow.cpp" line="1610"/>
        <location filename="../../gui/mainwindow.cpp" line="1631"/>
        <source>Can&apos;t write to file %1</source>
        <translation>Impossibile scrivere sul file %1</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1624"/>
        <source>Text file (*.txt);;Any file (*.*)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1662"/>
        <source>Display font</source>
        <translation>Font del display</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="2324"/>
        <source>System Default</source>
        <translation>Predefinita di &amp;Sistema</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="2328"/>
        <source>Language</source>
        <translation>Lingua</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="2328"/>
        <source>Select the language:</source>
        <translation>Selezionare la lingua</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="343"/>
        <source>&amp;Automatic</source>
        <translation>&amp;Automatico</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="574"/>
        <source>&amp;Precision</source>
        <translation>&amp;Precisione</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="290"/>
        <source>&amp;HTML</source>
        <translation>&amp;HTML</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="291"/>
        <source>Plain &amp;text</source>
        <translation>&amp;Testo in chiaro</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="310"/>
        <source>Formula &amp;Book</source>
        <translation>&amp;Formulario</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="356"/>
        <source>Check &amp;Updates</source>
        <translation>Verifica &amp;Aggiornamenti</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="357"/>
        <source>Send &amp;Feedback</source>
        <translation>Invia &amp;Feedback</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="358"/>
        <source>Join &amp;Community</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="359"/>
        <source>&amp;News Feed</source>
        <translation>&amp;News Feed</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="567"/>
        <source>&amp;Export</source>
        <translation>&amp;Esporta</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="578"/>
        <source>Color Scheme</source>
        <translation>Schema colori</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1344"/>
        <location filename="../../gui/mainwindow.cpp" line="1389"/>
        <source>SpeedCrunch Sessions (*.json);;All Files (*)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1362"/>
        <location filename="../../gui/mainwindow.cpp" line="1428"/>
        <source>Merge?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1602"/>
        <source>Export session as HTML</source>
        <translation>Esporta la sessione in HTML</translation>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1603"/>
        <source>HTML file (*.html)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/mainwindow.cpp" line="1623"/>
        <source>Export session as plain text</source>
        <translation>Esporta la sessione come testo semplice</translation>
    </message>
</context>
<context>
    <name>ManualWindow</name>
    <message>
        <location filename="../../gui/manualwindow.cpp" line="69"/>
        <source>SpeedCrunch Manual</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../gui/manualwindow.cpp" line="71"/>
        <source>%1 - SpeedCrunch Manual</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>UserFunctionListWidget</name>
    <message>
        <location filename="../../gui/userfunctionlistwidget.cpp" line="149"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../../gui/userfunctionlistwidget.cpp" line="149"/>
        <source>Value</source>
        <translation>Valore</translation>
    </message>
    <message>
        <location filename="../../gui/userfunctionlistwidget.cpp" line="152"/>
        <source>Search</source>
        <translation>Ricerca</translation>
    </message>
    <message>
        <location filename="../../gui/userfunctionlistwidget.cpp" line="153"/>
        <source>No match found</source>
        <translation>Nessuna Corrispondenza Trovata</translation>
    </message>
    <message>
        <location filename="../../gui/userfunctionlistwidget.cpp" line="155"/>
        <source>Insert</source>
        <translation>Inserisci</translation>
    </message>
    <message>
        <location filename="../../gui/userfunctionlistwidget.cpp" line="156"/>
        <source>Edit</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="../../gui/userfunctionlistwidget.cpp" line="157"/>
        <source>Delete</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="../../gui/userfunctionlistwidget.cpp" line="158"/>
        <source>Delete All</source>
        <translation>Cancella Tutto</translation>
    </message>
</context>
<context>
    <name>UserFunctionsDock</name>
    <message>
        <location filename="../../gui/userfunctionsdock.cpp" line="60"/>
        <source>User Functions</source>
        <translation>Funzione Utente</translation>
    </message>
</context>
<context>
    <name>VariableListWidget</name>
    <message>
        <location filename="../../gui/variablelistwidget.cpp" line="148"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../../gui/variablelistwidget.cpp" line="148"/>
        <source>Value</source>
        <translation>Valore</translation>
    </message>
    <message>
        <location filename="../../gui/variablelistwidget.cpp" line="151"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="../../gui/variablelistwidget.cpp" line="152"/>
        <source>No match found</source>
        <translation>Nessuna corrispondenza</translation>
    </message>
    <message>
        <location filename="../../gui/variablelistwidget.cpp" line="154"/>
        <source>Insert</source>
        <translation>Inserisci</translation>
    </message>
    <message>
        <location filename="../../gui/variablelistwidget.cpp" line="155"/>
        <source>Delete</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="../../gui/variablelistwidget.cpp" line="156"/>
        <source>Delete All</source>
        <translation>Cancella tutto</translation>
    </message>
</context>
<context>
    <name>VariablesDock</name>
    <message>
        <location filename="../../gui/variablesdock.cpp" line="59"/>
        <source>Variables</source>
        <translation>Variabili</translation>
    </message>
</context>
</TS>